export const WEAPON_FORM_STATUS = {
  wait_to_approve: 1,
  approved: 2,
  signed: 3,
  closed: 4,
};
