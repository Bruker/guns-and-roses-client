import React from "react";

export const AppContext = React.createContext({
  setUserName: () => {},
  setFormsToApprove: () => {},
  setFormsAdmitted: () => {},
  userName: "",
  formsToApprove: [],
  formsAdmitted: [],
});
