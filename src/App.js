import React, { Fragment, useState } from "react";
import { AppContext } from "./App.context";
import { Switch, Route } from "react-router-dom";
import GlobalAppBar from "./assets/global-app-bar";
import LoginPage from "./pages/login-page";
import AllWeaponFormsPage from "./pages/all-weapon-forms-page";
import Quiz from "./pages/quiz";
import CreateNewFormPage from "./pages/create-new-form-page";

const App = () => {
  const [user, setUser] = useState("");
  const [formsToApprove, setFormsToApprove] = useState([]);
  const [formsAdmitted, setFormsAdmitted] = useState([]);

  return (
    <Fragment>
      <GlobalAppBar />
      <AppContext.Provider
        value={{
          setUserName: setUser,
          setFormsAdmitted: setFormsAdmitted,
          setFormsToApprove: setFormsToApprove,
          formsAdmitted: formsAdmitted,
          formsToApprove: formsToApprove,
          userName: user,
        }}
      >
        <Switch>
          <Route exact path="/">
            <LoginPage />
          </Route>
          <Route exact path="/forms">
            <AllWeaponFormsPage />
          </Route>
          <Route exact path="/forms/new">
            <CreateNewFormPage />
          </Route>
          <Route path="/quiz">
            <Quiz />
          </Route>
        </Switch>
      </AppContext.Provider>
    </Fragment>
  );
};

export default App;
