import React, { Fragment, useContext } from "react";
import { AppContext } from "../../App.context";
import { Grid, makeStyles, Fab, Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import WeaponFormCategory from "./components/weapon-form-category";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles(() => ({
  allFormsPage: {
    marginTop: 10,
  },
  allFormsAddFab: {
    position: "fixed",
    bottom: 10,
    left: 10,
    width: 50,
    height: 50,
  },
}));

const AllWeaponFormsPage = () => {
  const { formsAdmitted, formsToApprove, userName } = useContext(AppContext);

  const classes = useStyles();
  const history = useHistory();

  const onAddButtonClicked = () => {
    history.push("/forms/new");
  };

  return (
    <Fragment>
      <Grid
        className={classes.allFormsPage}
        container
        direction="column"
        spacing={1}
      >
        <Grid item container justify="center" alignItems="center">
          <Grid item>
            <Typography
              variant="h4"
              color="primary"
            >{`שלום ${userName}`}</Typography>
          </Grid>
        </Grid>
        {/* {formsToApprove.length > 0 && (
          <Grid item>
            <WeaponFormCategory
              categoryWeaponForms={formsToApprove}
              categoryName="טפסים לאישורי"
            />
          </Grid>
        )} */}
        <Grid item>
          <WeaponFormCategory
            categoryWeaponForms={formsAdmitted}
            categoryName="טפסים שהגשתי"
          />
        </Grid>
      </Grid>
      <Fab
        className={classes.allFormsAddFab}
        color="primary"
        onClick={onAddButtonClicked}
      >
        <Add />
      </Fab>
    </Fragment>
  );
};

export default AllWeaponFormsPage;
