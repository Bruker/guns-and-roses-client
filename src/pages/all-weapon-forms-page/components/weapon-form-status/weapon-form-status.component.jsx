import React, { useEffect, useState } from "react";
import { makeStyles, Typography, Paper } from "@material-ui/core";
import { WEAPON_FORM_STATUS } from "../../../../App.constants";

const useStyles = makeStyles((theme) => ({
  waiting: {
    backgroundColor: theme.palette.primary,
  },
  approved: {
    backgroundColor: theme.palette.success,
  },
  signed: {
    backgroundColor: theme.palette.warning,
  },
  closed: {
    backgroundColor: theme.palette.grey,
  },
}));

const WeaponFormStatus = ({ status }) => {
  const classes = useStyles();
  const [statusInfo, setStatusInfo] = useState({
    className: classes.closed,
    text: "",
  });

  useEffect(() => {
    const getStatusInfo = (status) => {
      let returnedInfo = {};

      switch (status) {
        case WEAPON_FORM_STATUS.approved:
          returnedInfo = {
            color: "#4caf50",
            text: "אושר",
          };
          break;
        case WEAPON_FORM_STATUS.wait_to_approve:
          returnedInfo = {
            color: "#2196f3",
            text: "מחכה לאישור",
          };
          break;
        case WEAPON_FORM_STATUS.signed:
          returnedInfo = {
            color: "#ff9800",
            text: "נחתם",
          };
          break;
        case WEAPON_FORM_STATUS.closed:
          returnedInfo = {
            color: "#424242",
            text: "זוכה",
          };
          break;
        default:
          break;
      }

      return returnedInfo;
    };

    if (status) {
      const statusInfo = getStatusInfo(status);
      setStatusInfo(statusInfo);
    }
  }, [status, setStatusInfo]);

  return (
    <Paper
      elevation={6}
      style={{ backgroundColor: `${statusInfo.color}`, width: "150px" }}
    >
      <Typography variant="body1" align="center">
        {statusInfo.text}
      </Typography>
    </Paper>
  );
};

export default WeaponFormStatus;
