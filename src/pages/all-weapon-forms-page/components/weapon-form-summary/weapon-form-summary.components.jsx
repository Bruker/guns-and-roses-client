import React from "react";
import { Grid, Typography } from "@material-ui/core";
import WeaponFormStatus from "../weapon-form-status";

const WeaponFormSummary = ({ weaponForm }) => {
  return (
    <Grid container justify="flex-start" alignItems="center">
      <Grid item xs={10} container direction="column">
        <Grid item>
          <Typography variant="body1">{weaponForm.userName}</Typography>
        </Grid>
        <Grid item>
          <Typography variant="subtitle1">{weaponForm.startDate}</Typography>
        </Grid>
      </Grid>
      <Grid item>
        <WeaponFormStatus status={weaponForm.status} />
      </Grid>
    </Grid>
  );
};

export default WeaponFormSummary;
