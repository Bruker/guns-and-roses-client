import React, { useState, Fragment } from "react";
import {
  Accordion,
  AccordionSummary,
  Typography,
  List,
  ListItem,
  Divider,
} from "@material-ui/core";
import { ExpandMore } from "@material-ui/icons";
import WeaponFormSummary from "../weapon-form-summary";

const WeaponFormCategory = ({ categoryWeaponForms, categoryName }) => {
  const [isCatogeryExpanded, setIsCategoryExpanded] = useState(true);
  const categoryLength = categoryWeaponForms.length;

  const handleCategoryClick = () => {
    setIsCategoryExpanded((oldState) => !oldState);
  };

  return (
    <Accordion expanded={isCatogeryExpanded} onClick={handleCategoryClick}>
      <AccordionSummary expandIcon={<ExpandMore />}>
        <Typography variant="h6">{categoryName}</Typography>
      </AccordionSummary>
      <List>
        {categoryWeaponForms.map((weaponForm, weaponFormIndex) => (
          <Fragment key={weaponFormIndex}>
            <ListItem>
              <WeaponFormSummary weaponForm={weaponForm} />
            </ListItem>
            {weaponFormIndex + 1 < categoryLength && (
              <Divider key={`0${weaponFormIndex}`} />
            )}
          </Fragment>
        ))}
      </List>
    </Accordion>
  );
};

export default WeaponFormCategory;
