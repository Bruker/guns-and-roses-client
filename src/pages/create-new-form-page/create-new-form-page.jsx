import React, { useContext, useState } from "react";
import { AppContext } from "../../App.context";
import { Grid, Button, TextField } from "@material-ui/core";
import { WEAPON_FORM_STATUS } from "../../App.constants";
import { useHistory } from "react-router-dom";

const CreateNewFormPage = () => {
  const { setFormsAdmitted, setFormsToApprove } = useContext(AppContext);

  const [userName, setUserName] = useState("");
  const [reason, setReason] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const history = useHistory();

  const onTextFieldChanged = (event, dispatcherFunction) => {
    dispatcherFunction(event.target.value);
  };

  const onSubmit = () => {
    setFormsAdmitted((prevValue) => [
      ...prevValue,
      {
        userName,
        reason,
        startDate,
        endDate,
        status: WEAPON_FORM_STATUS.wait_to_approve,
      },
    ]);
    setFormsToApprove((prevValue) => [
      ...prevValue,
      {
        userName,
        reason,
        startDate,
        endDate,
        status: WEAPON_FORM_STATUS.wait_to_approve,
      },
    ]);

    history.push("/quiz");
  };

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      justify="center"
      spacing={1}
    >
      <Grid item>
        <TextField
          label="מספר אישי"
          type="decimal"
          onChange={(event) => onTextFieldChanged(event, setUserName)}
        />
      </Grid>
      <Grid item>
        <TextField
          label="סיבת בקשה"
          onChange={(event) => onTextFieldChanged(event, setReason)}
        />
      </Grid>
      <Grid item>
        <TextField
          label="תאריך קבלה"
          type="date"
          defaultValue={"2021-01-15"}
          onChange={(event) => onTextFieldChanged(event, setStartDate)}
        />
      </Grid>
      <Grid item>
        <TextField
          label="מספר החזרה"
          type="date"
          defaultValue={"2021-01-15"}
          onChange={(event) => onTextFieldChanged(event, setEndDate)}
        />
      </Grid>
      <Grid item>
        <Button variant="contained" color="primary" onClick={onSubmit}>
          פתיחת בקשה
        </Button>
      </Grid>
    </Grid>
  );
};

export default CreateNewFormPage;
