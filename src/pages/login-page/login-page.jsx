import React, { useContext } from "react";
import { AppContext } from "../../App.context";
import { makeStyles, Grid } from "@material-ui/core";
import LoginInput from "./components/login-input";
import LoginBackground from "../../assets/media/login_background.png";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles(() => ({
  loginPage: {
    height: "90vh",
  },
  LoginBackgroundImage: {
    height: 200,
    width: 250,
  },
}));

const LoginPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const { setUserName } = useContext(AppContext);

  const onLoginClicked = (userName) => {
    setUserName(userName);
    history.push("/forms");
  };

  return (
    <Grid
      direction="column"
      container
      justify="center"
      alignItems="center"
      className={classes.loginPage}
    >
      <Grid item>
        <img
          className={classes.LoginBackgroundImage}
          src={LoginBackground}
          alt="תמונה מגניבה"
        />
      </Grid>
      <Grid item>
        <LoginInput onSubmit={onLoginClicked} />
      </Grid>
    </Grid>
  );
};

export default LoginPage;
