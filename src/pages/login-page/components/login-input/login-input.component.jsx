import React, { useState } from "react";
import { TextField, Grid, Button } from "@material-ui/core";

const LoginInput = ({ onSubmit }) => {
  const [userName, setUserName] = useState("");

  const onUserNameChanged = (event) => {
    setUserName(event.target.value);
  };

  return (
    <Grid
      container
      direction="column"
      spacing={1}
      alignItems="center"
      justify="center"
    >
      <Grid item>
        <TextField
          label="שם משתמש"
          onChange={(event) => onUserNameChanged(event)}
        />
      </Grid>
      <Grid item>
        <TextField label="סיסמא" type="password" />
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          color="primary"
          onClick={() => onSubmit(userName)}
        >
          התחבר\י
        </Button>
      </Grid>
    </Grid>
  );
};

export default LoginInput;
