import React from "react";
import { Grid,FormControl,FormControlLabel,RadioGroup,Radio,Divider,FormLabel,Typography } from "@material-ui/core";

const Question = ( {Question} ) => {

  return (
    <Grid container direction="column" spacing={1} alignItems="right" justify="center" >
      
      <Grid item>
        <Typography color="primary" variant="h4">שאלה {Question.id}</Typography>
      </Grid>
      
      <Grid item sm={12}>
        <Divider />
      </Grid>

      <Grid item >
        <FormLabel component="legend">{Question.question}</FormLabel>
        <FormControl component="fieldset">
            <RadioGroup  name="answer1" /*value={value} onChange={handleChange}*/ >
                <FormControlLabel value="never" control={<Radio />} label={Question.wrongAns1} />
                <FormControlLabel value="terrorist" control={<Radio />} label={Question.wrongAns2}/>
                <FormControlLabel value="true"  control={<Radio />} label={Question.rightAns}/>
                <FormControlLabel value="always" control={<Radio />} label={Question.wrongAns3} />
            </RadioGroup>
        </FormControl>
      </Grid>
    </Grid>
  );
};

export default Question;