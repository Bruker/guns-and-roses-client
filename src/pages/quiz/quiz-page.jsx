import React from "react";
import {
  makeStyles,
  Grid,
  Typography,
  Button,
  MuiThemeProvider,
  createMuiTheme,
} from "@material-ui/core";
import Question from "./components/question";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles(() => ({
  quizPage: {
    height: "150vh",
    flexGrow: 1,
  },
}));

const QuizPage = () => {
  const classes = useStyles();
  const history = useHistory();

  const onFinish = () => {
    history.push("/forms");
  };

  const Questions = [
    {
      id: "1",
      question: "האם מותר להישען על קדח הקנה?",
      rightAns: "רק אם ממש עייפים",
      wrongAns1: "בשום פנים ואופן לא",
      wrongAns2: "אם מחבל אומר לך",
      wrongAns3: "בוודאי",
    },
    {
      id: "2",
      question: "מתי נפרוק את הנשק לבדיקה?",
      rightAns: "בסוף פעילות מבצעית",
      wrongAns1: "כל 6 שבועות",
      wrongAns2: "אחרי משיכת נשק מהנשקייה",
      wrongAns3: "כל התשובות נכונות",
    },
    {
      id: "3",
      question: "על מי מותר לכוון נשק?",
      rightAns: "על כל חולה קורונה",
      wrongAns1: "שליח פיצה שמגיע אחרי 30 דק",
      wrongAns2: "כן",
      wrongAns3: "אדם שזוהה כבעל אמעצי, כוונה ויכולת",
    },
  ];

  const btnTheme = createMuiTheme({ palette: { primary: { 500: "#108548" } } });

  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      spacing={2}
      className={classes.quizPage}
    >
      <Grid item>
        <Typography color="primary" variant="h2">
          בוחן בטיחות{" "}
        </Typography>
        <Typography color="textSecondary" variant="h4">
          טוב , בוא נראה כמה אתה מכיר את כללי הבטיחות בנשק...{" "}
        </Typography>
        <Question Question={Questions[0]} />
        <Question Question={Questions[1]} />
        <Question Question={Questions[2]} />
        <MuiThemeProvider theme={btnTheme}>
          <Button
            variant="contained"
            color="primary"
            disableElevation
            onClick={onFinish}
          >
            סיימתי!
          </Button>
        </MuiThemeProvider>
      </Grid>
    </Grid>
  );
};

export default QuizPage;
