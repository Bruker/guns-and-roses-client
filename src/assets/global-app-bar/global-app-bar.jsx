import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";

const GlobalAppBar = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6">Guns And Roses</Typography>
      </Toolbar>
    </AppBar>
  );
};

export default GlobalAppBar;
